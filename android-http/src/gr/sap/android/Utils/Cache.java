package gr.sap.android.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;
import java.util.Hashtable;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

public class Cache {
	private String TAG = "UI SAP";
	private Hashtable<String, String> mMemory;
	private Hashtable<String, Long> mMemoryTime;
	private Context mContext;
	private File mBase;
	private boolean mExternalStorageAvailable = false;
    private boolean mExternalStorageWriteable = false;
    private long MAXFILEAGE = 600000L; 
   
	
	public Cache(boolean mem){
		this.mBase = null;
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
		    mExternalStorageAvailable = true;
		    mExternalStorageWriteable = false;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		mMemory = new Hashtable<String, String>();
		mMemoryTime = new Hashtable<String, Long>();
	}
	
	public Cache(String base){
		this.mBase = new File(base);
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
		    mExternalStorageAvailable = true;
		    mExternalStorageWriteable = false;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		if (!this.mBase.exists()) this.mBase.mkdirs();
		mMemory = new Hashtable<String, String>();
		mMemoryTime = new Hashtable<String, Long>();
		ClearCache();
	}
	
	public String get(String key){
		String toreturn = "";
		if (mMemory.get(key)!=null){
			long lastmodified = mMemoryTime.get(key);
	    	long diff = Math.abs(System.currentTimeMillis()-lastmodified);
	    	if(diff>MAXFILEAGE) 
	    		toreturn="";
	    	else
	    		toreturn=mMemory.get(key);
		}else 
			toreturn="";
		
		if (toreturn.equals("")){
			toreturn=getFile(key);
			if (toreturn.equals("")){
				mMemory.put(key, toreturn);
				mMemoryTime.put(key, System.currentTimeMillis());
			}
		}
		return toreturn;
	}
	public String getFile(String key){
		FileInputStream fin = null;
		String ret = "";
		String filename = this.mBase+File.separator+key;
		File f = new File(filename);
		if (f.exists()){
			try {
				fin = new FileInputStream(filename);
				if (fin!=null) ret = Convert(fin);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return "";
			}
		}
		return ret;
	}
	private String Convert(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
 
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
	public void set(String key,String o){
		if (!o.equals("")){ 
			mMemory.put(key, (String)o);
			mMemoryTime.put(key, System.currentTimeMillis());
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(this.mBase+File.separator+key);
				fos.write(o.getBytes());
	        	fos.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	public void remove(String key){
		mMemory.remove(key);
	}
	public void ClearCache(){
		if (this.mBase!=null){
	    	File dir = new File(this.mBase.toString());
	    	File[] files = dir.listFiles();
	    	if (files!=null){
		    	for (File f : files ) {
		    	   Long lastmodified = f.lastModified();
		    	   long diff = Math.abs(System.currentTimeMillis()-lastmodified);
		    	   if(diff>MAXFILEAGE) {
		    	      f.delete();
		    	      Log.i(TAG,"Perform clean up "+basename.basename(f.toString())+" lastmod:"+lastmodified);
		    	   }
		    	}
	    	}
		}else 
			Log.i(TAG," Empty cache base ");
    }
}
