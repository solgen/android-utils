package gr.sap.android.Utils;

import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public class ImageUtil {
	private Context mContext;
	private Bitmap mBitmap;
	private Drawable mDrawable;
	private InputStream mContent;
	
	public ImageUtil(InputStream content){
		this.mContent = content;
	}

	public Context getmContext() {
		return mContext;
	}

	public void setmContext(Context mContext) {
		this.mContext = mContext;
	}

	public Bitmap getmBitmap() {
		if (this.mBitmap ==null)
			return BitmapFactory.decodeStream(mContent);
		else
			return this.mBitmap;
	}
	public void setBitmap(InputStream minput){
		this.mContent = minput;
		this.mBitmap = BitmapFactory.decodeStream(this.mContent);
	}
	public void setmBitmap(Bitmap mBitmap) {
		this.mBitmap = mBitmap;
	}

	public Drawable getmDrawable() {
		return mDrawable;
	}

	public void setmDrawable(Drawable mDrawable) {
		this.mDrawable = mDrawable;
	}

	public InputStream getmContent() {
		return mContent;
	}

	public void setmContent(InputStream mContent) {
		this.mContent = mContent;
	}
	
	public static Bitmap getResizedBitmap(Bitmap bm, int MaxSize) {
		if (bm == null ) return null;
		
		float scaleWidth,newHeight,scaleHeight,newWidth;
		int width = bm.getWidth();
		int height = bm.getHeight();
		Float ratio = ((float)width)/height;
		if (width>=height){
			scaleWidth = ((float) MaxSize) / width;
			newHeight = (float)(MaxSize/ratio);
			scaleHeight = ((float) newHeight) / height;
		}else{
			scaleHeight = ((float) MaxSize) / height;
			newWidth = (float)(MaxSize*ratio);
			scaleWidth = newWidth/width;
		}
		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
		return resizedBitmap;
	}
	public static Bitmap getCropResizedBitmap(Bitmap bm, int newWidth,int newHeight) {
		if (bm == null ) return null;
		bm = Bitmap.createScaledBitmap(bm,newWidth, newHeight,true);
		
		Paint paint = new Paint();
        paint.setFilterBitmap(true);
		RectF rectf = new RectF(0, 0, newWidth, newHeight);
        Canvas canvas = new Canvas();
        canvas.setBitmap(bm.copy(Bitmap.Config.ARGB_8888, true));
        Path path = new Path();               
        path.addRect(rectf, Path.Direction.CW);
        canvas.clipPath(path);
        //canvas.drawBitmap( bm, new Rect(0, 0, bm.getWidth(), bm.getHeight()), 
        //							new Rect(bm.getWidth()/2, bm.getHeight()/2, newWidth, newHeight), paint);
        int startleft = Math.abs(bm.getWidth()-newWidth)/2;
        int starttop = Math.abs(bm.getHeight()-newHeight)/2;
        canvas.drawBitmap(bm, startleft, starttop, paint);
        Matrix matrix = new Matrix();
        matrix.postScale(1f, 1f);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, startleft, starttop, newWidth, newHeight, matrix, true);
        //Log.i(TAG,"resized:"+resizedBitmap.getWidth()+"X"+resizedBitmap.getHeight());
        return resizedBitmap;
	}
}
