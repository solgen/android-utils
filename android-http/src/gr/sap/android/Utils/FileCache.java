package gr.sap.android.Utils;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.text.format.DateUtils;
import android.util.Log;

public class FileCache {
	private static final int BUFFER_SIZE = 8 * 1024;
	private static final String NOMEDIA_FILENAME = ".nomedia";
	private static final long CACHE_FILE_EXPIRATION = DateUtils.DAY_IN_MILLIS * 1;
	private static final String LOGCAT_NAME = "gr.sap.android.Utils.FileCache";
	private final Map< String, Object > firstLevelCache = Collections.synchronizedMap(new WeakHashMap<String, Object>());// Collections.synchronizedMap(new HashMap<String, Bitmap>());
	private int cacheMode = 0;
	private Context mContext;
	private File cacheFile;
	private BufferedInputStream in = null;
	
	public FileCache(Context mContext){//need context for finding the default cache dir of the phone.
		this.mContext = mContext;
		autoSelectCacheMode();
	}
	
	
	private final int autoSelectCacheMode(){
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			cacheMode = 0;
		}else
			cacheMode = 1;
		return cacheMode;
	}
	
	/* 
	 * Set the selected store method...
	 * 0 = SD with FirstLevelCache.
	 * 1 = Internal with FirstLevelCache.
	 * 2 = Disable first level cache and use SD only.
	 * 3 = First level cache only.
	 * By default this class will check upon creation if there is an SD 
	 * card. If there is this will be chosen else only use internal...
	 */
	public void setMode(int mode){
		this.cacheMode = mode;
	}
	
	
	private File getExternalStorageDir(String fileKey) {
		File extMediaDir = null;
		if (mContext != null && fileKey != null) {
			extMediaDir = new File(
				Environment.getExternalStorageDirectory() +"/Android/data/"+
				mContext.getPackageName());

			if (extMediaDir.exists()) {
				extMediaDir = createNomediaDotFile(extMediaDir,fileKey);
				return extMediaDir;
			}
			//Check again for SD state in case user has unmounted the SD will loading....
			if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
				File sdcard = Environment.getExternalStorageDirectory();

				if (sdcard.canWrite()) {
					extMediaDir.mkdirs();
					extMediaDir = createNomediaDotFile(extMediaDir,fileKey);
					return extMediaDir;
				} else {
					Log.e(LOGCAT_NAME, "SD card not writeable, unable to create directory: " + extMediaDir.getPath());
				}
			} else {
				return extMediaDir;
			}
		}
		return extMediaDir;
	}

	
	
	//Only used from getExternalStorageDir
	private File createNomediaDotFile( File directory , String fileKey) {
		File nomedia = new File(directory, fileKey+NOMEDIA_FILENAME);
		if (!nomedia.exists()) {
			try {
				nomedia.createNewFile();
			} catch (IOException e) {
				Log.e(LOGCAT_NAME, "unable to create .nomedia file in " + directory.getPath(), e);
			}
		}
		return nomedia;
	}

	//create and return a new Directory for storing...
	private File getInternalStorageDirectory(String key) {
		if (mContext != null) {
			File intCacheDir = new File(mContext.getCacheDir(), key);//create a file in the cache with name == KEY
			if (!intCacheDir.exists()) {
				try {
					intCacheDir.createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return intCacheDir;
		}
		return null;
	}
	
	//create and return a new Directory for storing...
	private File getInternalStorageDirectory() {
		if (mContext != null) {
			File intCacheDir = new File(mContext.getCacheDir().getAbsolutePath());
			if (!intCacheDir.exists()) {
				intCacheDir.mkdirs();
			}
			return intCacheDir;
		}
		return null;
	}
	
	
	
	public void put( Bitmap bitMap, String key) throws IOException{
		File cacheFile;
		switch (cacheMode) {
		case 0:
			firstLevelCache.put(key, bitMap);
			cacheFile = getExternalStorageDir( key);
			writeImage(cacheFile,bitMap);
			break;
		case 1:
			firstLevelCache.put(key, bitMap);
			cacheFile = getInternalStorageDirectory(key);
			writeImage(cacheFile,bitMap);
			break;

		default:
			break;
		}
	}
	
	public Bitmap get(String key) throws IOException, ClassNotFoundException{
		File cacheFile = null;
		Bitmap image = null;
		switch (cacheMode) {
		case 0:
			if (firstLevelCache.containsKey(key)){
				return (Bitmap) firstLevelCache.get(key);
			}else{
				cacheFile = getExternalStorageDir( key);
				image = readImage(cacheFile);
				if (image != null) firstLevelCache.put(key, image);
			}
			break;
		case 1:
			if (firstLevelCache.containsKey(key)){
				return (Bitmap) firstLevelCache.get(key);
			}else{
				cacheFile = getInternalStorageDirectory(key);
				image = readImage(cacheFile);
				if (image != null) firstLevelCache.put(key, image);
			}
			break;
			
		default:
			break;
		}
		return image;
	}
	
	private void writeImage(File cacheFile, Bitmap bitMap) throws FileNotFoundException{
		if(cacheFile != null){
		   FileOutputStream out = new FileOutputStream( cacheFile );
		   bitMap.compress(Bitmap.CompressFormat.PNG, 70, out);
		}
	}
	private Bitmap readImage(File cacheFile) throws IOException{
		if(cacheFile != null){
			in = new BufferedInputStream(new FileInputStream(cacheFile), BUFFER_SIZE);
			return BitmapFactory.decodeStream(in);
		}
		return null;
	}
	
	
	public void putObject( Object obj, String key) throws IOException{
		File cacheFile;
		switch (cacheMode) {
		case 0:
			firstLevelCache.put(key, obj);
			cacheFile = getExternalStorageDir( key);
			writeOBJ(cacheFile,obj);
			break;
		case 1:
			firstLevelCache.put(key, obj);
			cacheFile = getInternalStorageDirectory(key);
			writeOBJ(cacheFile,obj);
			break;

		default:
			break;
		}
	}
	public Object getObject(String key) throws IOException, ClassNotFoundException{
		File cacheFile = null;
		Object obj = null;
		switch (cacheMode) {
		case 0:
			if (firstLevelCache.containsKey(key)){
				return firstLevelCache.get(key);
			}else{
				cacheFile = getExternalStorageDir( key);
				obj = readOBJ(cacheFile,obj,key);
			}
			break;
		case 1:
			if (firstLevelCache.containsKey(key)){
				return (Object) firstLevelCache.get(key);
			}else{
				cacheFile = getInternalStorageDirectory(key);
				obj = readOBJ(cacheFile,obj,key);
			}
			break;
			
		default:
			break;
		}
		return obj;
	}

	
	private void writeOBJ(File cacheFile,Object obj) throws IOException{
		if(cacheFile != null){
		   FileOutputStream out = new FileOutputStream( cacheFile );
		   ObjectOutputStream objOut = new ObjectOutputStream(out);
		   objOut.writeObject(obj);
		   out.flush();
		   out.close();
		}
		
	}
	private Object readOBJ(File cacheFile,Object obj,String key) throws OptionalDataException, IOException, ClassNotFoundException{
		if(cacheFile != null){
			//in = new BufferedInputStream(new FileInputStream(cacheFile), BUFFER_SIZE);
			in = new BufferedInputStream(new FileInputStream(cacheFile));
			//fis = new FileInputStream(MessageCacheFile);
			obj = new ObjectInputStream(in).readObject();
			firstLevelCache.put(key,obj);
			in.close();
		}
		return obj;
	}

	public static boolean copyFileToFile(Context context, File src, File dst) {
		if (context != null && src != null && dst != null) {
			try {
				BufferedInputStream in = new BufferedInputStream(new FileInputStream(src), BUFFER_SIZE);
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(dst), BUFFER_SIZE);

				// Transfer bytes from in to out
				byte[] buf = new byte[BUFFER_SIZE];
				int len;

				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}

				in.close();
				out.close();
				return true;
			} catch (Exception e) {
				Log.e(LOGCAT_NAME, "unable to copy file " + src.getPath() + " to file " + dst.getPath(), e);
			}
		}
		
		return false;
	}
	
	public static boolean moveFileToFile(Context context, File src, File dst) {
		if (context != null && src != null && dst != null) {
			File extDir = Environment.getExternalStorageDirectory();
			File intDir = context.getFilesDir();

			// If src and dst are on the same filesystem, just renameTo()
			if ((src.getPath().startsWith(extDir.getPath()) && dst.getPath().startsWith(extDir.getPath())) ||
				(src.getPath().startsWith(intDir.getPath()) && dst.getPath().startsWith(intDir.getPath()))) {
				return src.renameTo(dst);
			}
			
			// Otherwise, copy and delete src
			if (copyFileToFile(context, src, dst)) {
				return src.delete();
			}
		}
		
		return false;
	}
    
	public static boolean copyUriToFile(Context context, Uri uri, File dst) {
		if (context != null && uri != null && dst != null) {
			try {
				BufferedInputStream in = new BufferedInputStream(context.getContentResolver().openInputStream(uri), BUFFER_SIZE);
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(dst), BUFFER_SIZE);
				// Transfer bytes from in to out
				byte[] buf = new byte[BUFFER_SIZE];
				int len;

				while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
				}

				in.close();
				out.close();
				return true;
			} catch (Exception e) {
				Log.e(LOGCAT_NAME, "unable to copy URI " + uri.toString() + " to file " + dst.getPath(), e);
			}
		}
		
		return false;
	}
	
	
	//Deletes all locally strored files (Both on SD and internal files) 
	//and also clears first level cache (HashMap)
	public void cleanCaches() {
		Log.i(LOGCAT_NAME, "cleaning up caches");
		clearInternal();
		clearSD();
		firstLevelCache.clear();
	}
	
	
	private void clearInternal(){
		File internalDir = getInternalStorageDirectory();
		
		if (internalDir != null) {
			File internalFiles[] = internalDir.listFiles();
	
			if (internalFiles != null && internalFiles.length > 0) {
				for (File file : internalFiles) {
					if (System.currentTimeMillis() - file.lastModified() >= CACHE_FILE_EXPIRATION) {
						Log.i(LOGCAT_NAME, "deleting " + file.getPath());
						file.delete();
					}
				}
			}
		}
	}
	private void clearSD(){
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			File externalDir = getExternalStorageDir( "/cache");
			
			if (externalDir != null) {
				File[] externalFiles = externalDir.listFiles();
				
				if (externalFiles != null && externalFiles.length > 0) {
					for (File file : externalFiles) {
						if (System.currentTimeMillis() - file.lastModified() >= CACHE_FILE_EXPIRATION && !NOMEDIA_FILENAME.equals(file.getName())) {
							Log.i(LOGCAT_NAME, "deleting " + file.getPath());
							file.delete();
						}
					}
				}
			}
		}
	}
	
	public void cleanSpecificCache(int[] caches){
		for( int cacheID : caches){
			if(cacheID == 0){
				clearInternal();
				firstLevelCache.clear();
			}else if(cacheID == 1){
				clearSD();
				firstLevelCache.clear();
			}else if(cacheID == 2){
				clearSD();
			}else
				firstLevelCache.clear();
		}
	}
	
	public static File createUniqueFile(File directory, String filename) {
		File file = new File(directory, filename);
		
		if (file != null && file.exists()) {
			// There is already a file here with the desired name, so let's
			// loop and create a unique one with a numeric suffix.
			// 
			// This is awful, find a library that does this for us.
			try {
				int index = 0;
				
				while (file.exists() && index <= 128) {
					file = new File(directory, "[" + (++index) + "]" + filename);
				}
				
				if (file.exists()) {
					file = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				file = null;
			}
		}
		
		return file;
	}
}